package rxtutorial.chapter4;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;

public class Example17 {
	
	public static void main(String[] args) {
		
		Observable<Integer> observable = createObservable()
			.filter(i->i > 50 && i < 60)
			.takeLast(3);
		
		observable.subscribe(createObserver());
		
		System.out.println("Terminando.");
		
	}
	
	public static Observable<Integer> createObservable() {
		return Observable.create(new Observable.OnSubscribe<Integer>() {

			@Override
			public void call(Subscriber<? super Integer> t) {
				for (int i = 0; i < 100; i++) {
					t.onNext(i);
				}
				t.onCompleted();
			}
			
		});
	}
	
	public static Observer<Integer> createObserver() {
		return new Observer<Integer>() {

			@Override
			public void onError(Throwable e) {
				System.out.println("onError");
			}

			@Override
			public void onCompleted() {
				System.out.println("onCompleted");
			}
			
			@Override
			public void onNext(Integer l) {
				System.out.println(l);
			}

		};
	}
	
}