package rxtutorial.chapter4;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;

public class Example19 {
	
	public static void main(String[] args) {
		
		Observable<Integer> observable = createObservable()
				.distinctUntilChanged();
		
		observable.subscribe(createObserver());
		
		System.out.println("Terminando.");
		
	}
	
	public static Observable<Integer> createObservable() {
		return Observable.create(new Observable.OnSubscribe<Integer>() {

			@Override
			public void call(Subscriber<? super Integer> t) {
				for (int i = 0; i < 3; i++) {
					t.onNext(1);
				}
				for (int i = 0; i < 3; i++) {
					t.onNext(2);
				}
				for (int i = 0; i < 3; i++) {
					t.onNext(3);
				}
				for (int i = 0; i < 3; i++) {
					t.onNext(1);
				}
				t.onCompleted();
			}
			
		});
	}
	
	public static Observer<Integer> createObserver() {
		return new Observer<Integer>() {

			@Override
			public void onError(Throwable e) {
				System.out.println("onError");
			}

			@Override
			public void onCompleted() {
				System.out.println("onCompleted");
			}
			
			@Override
			public void onNext(Integer l) {
				System.out.println(l);
			}

		};
	}
	
}