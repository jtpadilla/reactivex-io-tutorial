package rxtutorial.chapter4;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;

public class Example23 {
	
	public static void main(String[] args) {
		
		Observable<Integer> observable = createObservable()
				.debounce(2500, TimeUnit.MILLISECONDS);
		
		observable.subscribe(createObserver());
		
		System.out.println("Terminando.");
		
	}
	
	public static Observable<Integer> createObservable() {
		return Observable.create(new Observable.OnSubscribe<Integer>() {

			@Override
			public void call(Subscriber<? super Integer> t) {
				for (int i = 0; i < 10; i++) {
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
					}
					t.onNext(i);
					System.out.println("Se emite " + i);
				}
				for (int i = 0; i < 5; i++) {
					try {
						TimeUnit.SECONDS.sleep(2);
					} catch (InterruptedException e) {
					}
					t.onNext(i);
					System.out.println("Se emite " + i);
				}
				for (int i = 0; i < 5; i++) {
					try {
						TimeUnit.SECONDS.sleep(3);
					} catch (InterruptedException e) {
					}
					t.onNext(i);
					System.out.println("Se emite " + i);
				}
				t.onCompleted();
			}
			
		});
	}
	
	public static Observer<Integer> createObserver() {
		return new Observer<Integer>() {

			@Override
			public void onError(Throwable e) {
				System.out.println("onError");
			}

			@Override
			public void onCompleted() {
				System.out.println("onCompleted");
			}
			
			@Override
			public void onNext(Integer l) {
				System.out.println(l);
			}

		};
	}
	
}