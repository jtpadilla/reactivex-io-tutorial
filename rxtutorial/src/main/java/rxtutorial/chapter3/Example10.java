package rxtutorial.chapter3;

import rx.Observable;
import rx.Observer;

public class Example10 {
	
	public static void main(String[] args) {
		new Example10().test();
	}
	
	public void test() {
		
		Observable<Integer> observable = Observable.defer(this::createObservable);
		observable.subscribe(createObserver("uno"));
		observable.subscribe(createObserver("dos"));
		
	}
	
	public Observable<Integer> createObservable() {
		return Observable.create(subscriber -> {
				if (subscriber.isUnsubscribed()) {
					return;
				}
				System.out.println("getint");
				subscriber.onNext(1);
				subscriber.onCompleted();
		});
	}
	
	public Observer<Integer> createObserver(String name) {
		return new Observer<Integer>() {

			@Override
			public void onError(Throwable e) {
			}

			@Override
			public void onCompleted() {
			}
			
			@Override
			public void onNext(Integer i) {
				System.out.println(name + " -- > " + i);
			}

		};
	}
	
}