package rxtutorial.chapter3;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;

public class Example14 {
	
	public static void main(String[] args) {
		
		Observable<Long> observable = createObservable();
		observable.subscribe(createObserver());
		
		try {
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Terminando.");
		
	}
	
	public static Observable<Long> createObservable() {
		return Observable.timer(1, 3, TimeUnit.SECONDS);
	}
	
	public static Observer<Long> createObserver() {
		return new Observer<Long>() {

			@Override
			public void onError(Throwable e) {
				System.out.println("onError");
			}

			@Override
			public void onCompleted() {
				System.out.println("onCompleted");
			}
			
			@Override
			public void onNext(Long l) {
				System.out.println(l);
			}

		};
	}
	
}