package rxtutorial.chapter3;

import rx.Observable;
import rx.Observer;

public class Example11 {
	
	public static void main(String[] args) {
		
		Observable<Integer> observable = createObservable();
		observable.subscribe(createObserver("uno"));
		observable.subscribe(createObserver("dos"));
		
	}
	
	public static Observable<Integer> createObservable() {
		return Observable.just(1, 2, 3)
			.repeat(3);
	}
	
	public static Observer<Integer> createObserver(String name) {
		return new Observer<Integer>() {

			@Override
			public void onError(Throwable e) {
			}

			@Override
			public void onCompleted() {
			}
			
			@Override
			public void onNext(Integer i) {
				System.out.println(name + " -- > " + i);
			}

		};
	}
	
}