package rxtutorial.chapter5;

import rx.Observable;
import rx.Subscriber;

public class Example30 {

    public static void main(String[] args) {
    	Observable.just(1, 2, 3, 4, 5)
    	.scan((sum, item) -> {
    			System.out.println(String.format("%d + %d", sum, item));
    			return sum + item;
    		})
    	.subscribe(new Subscriber<Integer>() {

			@Override
			public void onNext(Integer t) {
				System.out.println(t);
			}
			@Override
			public void onCompleted() {
				System.out.println("Completed");
			}

			@Override
			public void onError(Throwable e) {
				e.printStackTrace();
			}

		});
    	
    }

}