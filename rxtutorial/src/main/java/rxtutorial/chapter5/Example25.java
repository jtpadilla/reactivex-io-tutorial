package rxtutorial.chapter5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;

public class Example25 {

	public static void main(String[] args) {
		new Example25();
	}

	private Example25() {
		Observable.merge(createIntegerObservables())
			.subscribe(createIntegerObserver());
	}
	
	///////////////////////////////////////
	// Observable
	///////////////////////////////////////
	
	static private final List<Integer> numbers = new ArrayList<>(Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9, 10));

	public Observable<Observable<Integer>> createIntegerObservables() {
		return Observable.create(new Observable.OnSubscribe<Observable<Integer>>() {
			@Override
			public void call(Subscriber<? super Observable<Integer>> t) {
				for (int i = 0; i < 5; i++) {
					t.onNext(createIntegerObservable());
				}
				t.onCompleted();
			}
		});
	}
	
	public Observable<Integer> createIntegerObservable() {
		return Observable.from(numbers);
	}
	
	///////////////////////////////////////
	// Observer
	///////////////////////////////////////
	
	public Observer<Integer> createIntegerObserver() {
		return new Observer<Integer>() {

			@Override
			public void onError(Throwable e) {
				System.out.println("onError");
			}

			@Override
			public void onCompleted() {
				System.out.println("onCompleted");
			}
			
			@Override
			public void onNext(Integer o) {
				System.out.println(o);
			}

		};
	}

}