package rxtutorial.chapter5;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;

public class Example28 {

	public static void main(String[] args) {
		new Example28();
	}
	
	private Example28() {
		createObservable()
			.concatMap(v -> Observable.just(v).delay(11 - v, TimeUnit.SECONDS))
			.toBlocking()
			.subscribe(createObserver());
	}
	
	///////////////////////////////////////
	// Observable
	///////////////////////////////////////
	
	public Observable<Integer> createObservable() {
		return Observable.range(1,  10);
	}
	
	///////////////////////////////////////
	// Observer
	///////////////////////////////////////
	
	public Observer<Integer> createObserver() {
		return new Observer<Integer>() {

			@Override
			public void onError(Throwable e) {
				System.out.println("onError");
			}

			@Override
			public void onCompleted() {
				System.out.println("onCompleted");
			}
			
			@Override
			public void onNext(Integer o) {
				System.out.println(o);
			}

		};
	}

}