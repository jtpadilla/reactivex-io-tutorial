package rxtutorial.chapter5;

import rx.Observable;
import rx.Observer;

public class Example26 {

	public static void main(String[] args) {
		new Example26();
	}
	
	private Example26() {
		createObservable()
			.flatMap(v -> Observable.range(v, 2))
			.subscribe(createObserver());
	}
	
	///////////////////////////////////////
	// Observable
	///////////////////////////////////////
	
	public Observable<Integer> createObservable() {
		return Observable.range(1,  10);
	}
	
	///////////////////////////////////////
	// Observer
	///////////////////////////////////////
	
	public Observer<Integer> createObserver() {
		return new Observer<Integer>() {

			@Override
			public void onError(Throwable e) {
				System.out.println("onError");
			}

			@Override
			public void onCompleted() {
				System.out.println("onCompleted");
			}
			
			@Override
			public void onNext(Integer o) {
				System.out.println(o);
			}

		};
	}

}