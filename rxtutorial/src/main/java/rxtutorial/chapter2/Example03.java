package rxtutorial.chapter2;
import java.util.Arrays;
import java.util.List;

import rx.Observable;
import rx.Observer;

public class Example03 {
	
	public static void main(String[] args) {
		
		List<String> list = Arrays.asList("Android", "Ubuntu", "Mac OS");
		
		Observable<List<String>> listObservable = Observable.just(list);
		
		listObservable.subscribe(new Observer<List<String>>() {

			@Override
			public void onError(Throwable e) {
			}

			@Override
			public void onNext(List<String> list) {
				System.out.println(list);
			}

			@Override
			public void onCompleted() {
			}
			
		});
		
	}
}