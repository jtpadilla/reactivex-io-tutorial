package rxtutorial.chapter2;

import rx.Observer;
import rx.Subscription;
import rx.subjects.PublishSubject;

public class Example04 {
	
	public static void main(String[] args) {
		
		PublishSubject<String> stringPublishSubject = PublishSubject.create();

		Subscription subscriptionPrint = stringPublishSubject.subscribe(new Observer<String>() {

			@Override
			public void onError(Throwable e) {
				System.out.println("Ha ocurrido un error");
			}

			@Override
			public void onNext(String list) {
				System.out.println(list);
			}

			@Override
			public void onCompleted() {
				System.out.println("Ha finalizado");
			}
			
		});
		
		stringPublishSubject.onNext("Hola mundo");
		
	}
}