package rxtutorial.chapter2;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Action0;
import rx.subjects.PublishSubject;

public class Example05 {
	
	public static void main(String[] args) {
		
		PublishSubject<Boolean> subject = PublishSubject.create();

		subject.subscribe(new Observer<Boolean>() {

			@Override
			public void onError(Throwable e) {
			}

			@Override
			public void onCompleted() {
			}
			
			@Override
			public void onNext(Boolean list) {
				System.out.println("Observable completed!");
			}

		});

		Observable.create(new Observable.OnSubscribe<Integer>() {
			
			@Override
			public void call(Subscriber<? super Integer> subscriber) {
				for (int i = 0; i < 5; i++) {
					subscriber.onCompleted();
				}
				
			}
			
		}).doOnCompleted(new Action0() {
			
			@Override
			public void call() {
				subject.onNext(true);
			}
			
		}).subscribe();
		
	}
}