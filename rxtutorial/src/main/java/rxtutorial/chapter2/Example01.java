package rxtutorial.chapter2;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;


public class Example01 {

	public static void main(String[] args) {
		
		List<Integer> items = new ArrayList<Integer>();
		items.add(1);
		items.add(10);
		items.add(100);
		items.add(200);
		
		Observable<Integer> observable = Observable.from(items);
		
		Subscription subscriptionPrint = observable.subscribe(new Observer<Integer>() {

			@Override
			public void onNext(Integer value) {
				System.out.println(value);
			}

			@Override
			public void onError(Throwable e) {
				System.out.println("onError()");
			}

			@Override
			public void onCompleted() {
				System.out.println("onCompleted()");
			}
			
		});
		
	}

}
