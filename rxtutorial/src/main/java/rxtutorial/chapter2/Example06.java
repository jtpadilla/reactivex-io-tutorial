package rxtutorial.chapter2;

import rx.Observer;
import rx.subjects.BehaviorSubject;

public class Example06 {
	
	public static void main(String[] args) {
		
		BehaviorSubject<Integer> subject = BehaviorSubject.create(0);
		subject.subscribe(createObserver("A"));
		subject.onNext(1);
		subject.onNext(2);
		System.out.println();
		subject.subscribe(createObserver("B"));
		System.out.println();
		subject.onNext(3);
		subject.onNext(4);
		
	}
	
	public static Observer<Integer> createObserver(String name) {
		return new Observer<Integer>() {

			@Override
			public void onError(Throwable e) {
			}

			@Override
			public void onCompleted() {
			}
			
			@Override
			public void onNext(Integer i) {
				System.out.println(name + " -- > " + i);
			}

		};
	}
	
}