package rxtutorial.chapter2;
import rx.Observable;
import rx.Observer;

public class Example02 {
	
	public static void main(String[] args) {
		
		Observable<String> observableString = Observable.just(helloWorld());
		
		observableString.subscribe(new Observer<String>() {

			@Override
			public void onError(Throwable e) {
			}

			@Override
			public void onNext(String str) {
				System.out.println(str);
			}

			@Override
			public void onCompleted() {
			}
			
		});
		
	}

	private static String helloWorld() {
		return "Hola";
	}
}