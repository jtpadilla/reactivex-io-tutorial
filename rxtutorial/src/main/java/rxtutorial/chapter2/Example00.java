package rxtutorial.chapter2;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;


public class Example00 {

	public static void main(String[] args) {
		
		Observable<Integer> observable = Observable.create(new Observable.OnSubscribe<Integer>() {

			@Override
			public void call(Subscriber<? super Integer> t) {
				for (int i = 0; i < 5; i++) {
					t.onNext(i);
				}
				t.onCompleted();
			}
		});
		
		Subscription subscriptionPrint = observable.subscribe(new Observer<Integer>() {

			@Override
			public void onNext(Integer value) {
				System.out.println(value);
			}

			@Override
			public void onError(Throwable e) {
				System.out.println("onError()");
			}

			@Override
			public void onCompleted() {
				System.out.println("onCompleted()");
			}
			
		});
		
	}

}
