package rxtutorial.chapter6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rx.Observable;
import rx.Observer;

public class Example31 {

	public static void main(String[] args) {
		new Example31();
	}

	private Example31() {
		Observable.merge(createIntegerObservableList())
			.subscribe(createObserver());
	}
	
	///////////////////////////////////////
	// Observable
	///////////////////////////////////////
	
	static private final List<Integer> numbers = new ArrayList<>(Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9, 10));

	public List<Observable<Integer>> createIntegerObservableList() {
		ArrayList<Observable<Integer>> resultList = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			resultList.add(createIntegerObservable());
		}
		return resultList;
	}
	
	public Observable<Integer> createIntegerObservable() {
		return Observable.from(numbers);
	}
	
	///////////////////////////////////////
	// Observer
	///////////////////////////////////////
	
	public Observer<Integer> createObserver() {
		return new Observer<Integer>() {

			@Override
			public void onError(Throwable e) {
				System.out.println("onError");
			}

			@Override
			public void onCompleted() {
				System.out.println("onCompleted");
			}
			
			@Override
			public void onNext(Integer l) {
				System.out.println(l);
			}

		};
	}

}